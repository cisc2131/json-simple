package JsonLoginFolder;
import java.io.IOException;
import javax.swing.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * updates to this from the simple login project are indicated in the below comments
 * @author Nick Tagliamonte
 */
public class JsonLogin{
	static JsonLoginObject login;
	static JSONObject credentialObject;
	static JSONObject currentCredential;
	static JSONArray credentialList;
	
	public static void main(String[] args) throws IOException, ParseException{
		//This code block creates and displays the login popup to get user input
		String inputID;
		String inputPassword;
		JTextField IDField = new JTextField(20);
	    JPasswordField passwordField = new JPasswordField(20);
	    JPanel loginPanel = new JPanel();
	    loginPanel.add(new JLabel("ID: "));
	    loginPanel.add(IDField);
	    loginPanel.add(new JLabel("Password: "));
	    loginPanel.add(passwordField);
		int getCredentials = JOptionPane.showConfirmDialog(null, loginPanel, "Please login", JOptionPane.OK_CANCEL_OPTION);
		
		//If credentials were entered in the popup created above, they are verified against a list of user credentials 
		if (getCredentials == JOptionPane.OK_OPTION) {
			inputID = IDField.getText();
			inputPassword = new String(passwordField.getPassword());
		}
		else {
			inputID = null;
			inputPassword = null;
		}
		
		//create a JSONArray of all the sets of credentials
		credentialList = JsonIO.getJsonList("credentials.json");
		
		//iterate through the credentialList JSONArray and pull individual json objects from it.
		for(int i = 0; i < credentialList.size(); i++) {
			currentCredential = (JSONObject) credentialList.get(i);	
			credentialObject = (JSONObject) currentCredential.get("credential");
			
			//on a match to the user-input ID, this creates a new login object.  From here the program is unchanged until the resetPAssword method
			String ID = (String) credentialObject.get("ID");
			if (ID.equals(inputID)) {
				String password = (String) credentialObject.get("password");
				String maidenName = (String) credentialObject.get("maiden name");
				String petName = (String) credentialObject.get("pet name");
				login = new JsonLoginObject(ID, password, maidenName, petName);
				break;
			}
		}
		
		if(verifyPassword(inputPassword))
			JOptionPane.showMessageDialog(null, "This is the application");
		}
	
	/**
	 * The verifyPassword method checks the password entered by the user against the password stored in the login object
	 * If they match in 3 or fewer tries then control goes back to main, otherwise the motherVerification method is called
	 * @param inputPassword
	 * @return true (or false) depending on whether the user input matc hes the credentials file
	 * @throws IOException
	 * @throws ParseException 
	 */
	private static boolean verifyPassword(String inputPassword) throws IOException, ParseException {
		String password = inputPassword;
		//This while loop allows the user to re-enter the password if it was incorrect at first
		while (login.getAttempts() < 2 && !inputPassword.equals(login.getPassword())) {
			login.incrementAttempts();
			JOptionPane.showMessageDialog(null, "Incorrect password, please try again");
			password = JOptionPane.showInputDialog(null, "Password: ");
		}
		if (login.getAttempts() == 2) {
			JOptionPane.showMessageDialog(null, "Too many incorrect attemps, please complete verification and set a new password");
			motherVerification();
		}
		return (password.equals(login.getPassword()));
	}

	/**
	 * The motherVerification method just checks if the user input mothers maiden name matches what is in the login object.
	 * If it does, then the verifyPet method is called
	 * Otherwise the program exits
	 * @throws IOException
	 * @throws ParseException 
	 */
	private static void motherVerification() throws IOException, ParseException {
		String maidenName = JOptionPane.showInputDialog(null, "Enter your Mother's maiden name: ");
		if (maidenName.equals(login.getMaidenName())){
			verifyPet();
		}
		else {
			JOptionPane.showMessageDialog(null, "Too many incorrect attemps, login will now exit");
			System.exit(0);
		}
	}

	/**
	 * The verifyPet method just checks if the user input pet name matches what is in the login object.
	 * If it does, then the resetPassword method is called
	 * Otherwise the program exits
	 * @throws IOException
	 * @throws ParseException 
	 */
	private static void verifyPet() throws IOException, ParseException {
		String petName = JOptionPane.showInputDialog(null, "Enter your pet's name: ");
		if (petName.equals(login.getPet())){
			resetPassword();
		}
		else {
			JOptionPane.showMessageDialog(null, "Too many incorrect attemps, login will now exit");
			System.exit(0);
		}
	}

	/**
	 * this method adds the updates to the JSONIO generic object and then calls the writeFileFromArray method with the pre existing JSONArray updated to show the new information
	 * @throws IOException
	 * @throws ParseException 
	 */
	private static void resetPassword() throws IOException, ParseException {
		String password = JOptionPane.showInputDialog(null, "Enter a new password: ");
		String passwordVerify = JOptionPane.showInputDialog(null, "Verify New Password: ");
		if (password.equals(passwordVerify)) {
			login.setAttempts(0);
			login.setPassword(password);

			credentialList.remove(currentCredential);
			credentialList.remove(credentialObject);
			credentialObject.replace("password", password);
			credentialList.add(currentCredential);
			JsonIO.writeFileFromArray(credentialList, "credentials.json");
			
			main(null);
		}
		else
			JOptionPane.showMessageDialog(null, "Passwords do not match, login will now exit");
			System.exit(0);
	}
}