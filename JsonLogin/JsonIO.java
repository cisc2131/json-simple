package JsonLoginFolder;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * Project Json Simple
 * Author Nick Tagliamonte
 * Created 2/23
 * This IO class contains a method to get a generic object from a json file, a method to get a jsonlist from a json file, and methods to add KV pairs to a generic jsonobject and then write that object to a json file
 */
public class JsonIO {
	static JSONObject genericObject = new JSONObject();

	public Object readFromJson(String filename, Object className) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Object returnObject = objectMapper.readValue(new File(filename), className.getClass());
		return returnObject;
	}
	
	public static JSONArray getJsonList(String filename) throws IOException, ParseException {
		JSONParser parser = new JSONParser();
		FileReader reader = new FileReader(filename);
		Object currentObject= parser.parse(reader);
		JSONArray returnList = (JSONArray) currentObject;
		return returnList;
	}
	
	public static void addSingleValue(String key, String value) {
		genericObject.put(key, value);
	}
	
	public void addArray(String key, String[] array) {
		JSONArray newArray = new JSONArray();
		for (int i = 0; i < array.length; i++) {
			newArray.add(array[i]);
		}
		genericObject.put(key, newArray);
	}
	
	public static void writeChangeToFile(String filename) throws IOException {
		Files.write(Paths.get(filename), genericObject.toJSONString().getBytes());
	}
	
	public static void writeFileFromArray(JSONArray array, String filename) throws IOException {
		FileWriter file = new FileWriter(filename);
		file.write(array.toJSONString());
		file.close();
	}
}
