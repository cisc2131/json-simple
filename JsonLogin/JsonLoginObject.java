package JsonLoginFolder;
/**
 * Project Simple Login
 * Author Nick Tagliamonte
 * Created 1/22/23
 * Description
 * The Login class contains a constructor and fields for the ID, Password, Mother's Maiden Name, Pet Name, and number of login attempts.
 * It has getters and setters for all fields, a method to increment the login attempts, and a toString method to display the values in each field.
 */
public class JsonLoginObject {
	public String ID;
	public String password;
	public String maidenName;
	public String pet;
	private int attempts;
	
	/**
	 * This constructor has an argument for all  the login class fields and also sets attempts to 0
	 * @param ID
	 * @param password
	 * @param maidenName
	 * @param Pet
	 */
	public JsonLoginObject (String ID, String password, String maidenName, String Pet) {
		super();
		this.ID = ID;
		this.password = password;
		this.maidenName = maidenName;
		this.pet = Pet;
		attempts = 0;
	}
	public JsonLoginObject () {}
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMaidenName() {
		return maidenName;
	}
	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}
	public String getPet() {
		return pet;
	}
	public void setPet(String pet) {
		this.pet = pet;
	}
	public int getAttempts() {
		return attempts;
	}
	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}
	
	/**
	 * The incrementAttempts method just increases the number of attempts by 1
	 */
	public void incrementAttempts() {
		attempts++;
	}
	
	/**
	 * The toString method returns all of the firlds including attempts, seperated by commas
	 */
	@Override
	public String toString() {
		return ID + "," + password + "," + maidenName + "," + pet + "," + attempts;
	}
}