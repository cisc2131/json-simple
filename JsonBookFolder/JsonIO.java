package JsonBookFolder;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.cliftonlabs.json_simple.Jsoner;

/**
 * Project Json Simple
 * Author Nick Tagliamonte
 * Created 2/23
 * This IO class contains a method to get a List of books from a json file (ObjectMapper is used to create items from the json string)
 * and a method to write a jsonarray to a file.  The jsonarray is generated in the main method and passed in here
 */
public class JsonIO {
	
	public static List<Book> getJsonList(String filename) throws IOException, ParseException {
		ObjectMapper mapper = new ObjectMapper();
		List<Book> recipes = Arrays.asList(mapper.readValue(Paths.get(filename).toFile(), Book[].class));
		return recipes;
	}
	
	public static void writeToFile(JSONArray object, String filename) throws IOException {
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
		Jsoner.serialize(object, writer);
		writer.close();
	}
}
