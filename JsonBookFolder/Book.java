package JsonBookFolder;
/**
 * Project Json Simple
 * Author Nick Tagliamonte
 * Created 2/23
 * The book class is a simple class with fields, one of which is an array, to demonstrate json IO
 */

import java.util.Arrays;
import java.util.Objects;


public class Book {

    private String title;
    private String isbn;
    private long year;
	private String[] authors;

    public Book() {
    }

    public Book(String title, String isbn, long year, String[] authors) {
        this.title = title;
        this.isbn = isbn;
        this.year = year;
        this.authors = authors;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public long getYear() {
		return year;
	}

	public void setYear(long year) {
		this.year = year;
	}

	public String[] getAuthors() {
		return authors;
	}

	public void setAuthors(String[] authors) {
		this.authors = authors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(authors);
		result = prime * result + Objects.hash(isbn, title, year);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return Arrays.equals(authors, other.authors) && Objects.equals(isbn, other.isbn)
				&& Objects.equals(title, other.title) && year == other.year;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", isbn=" + isbn + ", year=" + year + ", authors=" + Arrays.toString(authors)
				+ "]";
	}   
}