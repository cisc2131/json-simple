package JsonBookFolder;
/**
 * Project Json Simple
 * Author Nick Tagliamonte
 * Created 2/23
 * This class contains the main method to test that the book class can write to and read from json files
 */


import java.util.Arrays;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class BookTest {
	static List<Book> books = null;

	public static void main(String[] args) throws Exception {
		
		JSONObject obj = new JSONObject();
		obj.put("title", "Title3");
		obj.put("isbn", "#####");
		obj.put("year", 2024);
		
		JSONArray arr = new JSONArray();
		arr.addAll(Arrays.asList("Author1", "Author2"));
		obj.put("authors", arr);
		
		JSONArray arr2 = new JSONArray();
		arr2.add(obj);
		
		JsonIO.writeToFile(arr2, "book.json");
		
		try {
			books = JsonIO.getJsonList("book.json");
			books.forEach(System.out::println);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
